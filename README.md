# BSH

Este es el esqueleto de un shell más o menos usable, el `bsh` (berreta shell).

## Build

```bash
make
```

Es necessario tener instalada la librería `readline`, en ubuntu la instalamos con:

```bash
sudo apt-get install libreadline-dev
```

Si no estuviesen instaladas las herramientas de desarrollo de linux, ejecutar:

```bash
sudo apt-get install build-essential
```

## Ejecución

```bash
./bsh
```

## Mejoras Propuestas

1.  Manejar la señal Ctrl-C. (ver `man signal` - _SIGINT_)
2.  Mostrar en el prompt el direcotrio actual (ver implemenatción `mypwd`)
3.  Implementar un comando built-in `export` que agregue variables de entorno: `export TEST=test`. (ver `man setenv`)
4.  Implementar el comando built-in myls que muestre el contenido de un directorio. (ver `man opendir` y `man readdir`)
5.  Implementar una comando built-in "concr comando" que reciba como parámetro un comando con sus argumentos y lo ejecute en background.
6.  Idem 4 pero con la posibilidad de separar 1 o más comandos con "&".
7.  Implementar el operador `|` (pipe) que redireccione la salida de un comando a la entrada del siguiente. (ver `man popen`, `man dup` y `man pipe`)
8.  Proponer alguna extensión interesante!
